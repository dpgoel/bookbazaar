Prerequisite

    Java 1.8
    JDK 13.0.1
    Kafka 2.4.0
    Maven 3.6.3
    PostgreSQL 12.1

Service registry (Eureka)

    Build/Run
        mvn clean install
        java -jar target/discovery-0.0.1-SNAPSHOT.jar
    Check
        http://localhost:8761/



package com.devpriya.user.service;

import com.devpriya.user.db.UserRepository;
import com.devpriya.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface UserService {

    User registerUser(User input);

    Iterable<User> findAllUsers();

    void deleteUser(Long id);

    User findById(Long id);
}

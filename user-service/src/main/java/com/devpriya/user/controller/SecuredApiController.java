package com.devpriya.user.controller;

import com.devpriya.user.model.User;
import com.devpriya.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/secured")
@CrossOrigin
public class SecuredApiController {
    private UserService userService;

    @Autowired
    public SecuredApiController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "admin/all_users", method = RequestMethod.GET)
    public ResponseEntity<Iterable<User>> getAll() {
        return new ResponseEntity<>(userService.findAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "admin/create_user", method = RequestMethod.POST)
    public ResponseEntity<User> create(@RequestBody User user) {
        return new ResponseEntity<>(userService.registerUser(user), HttpStatus.OK);
    }

    @RequestMapping(value = "admin/delete_user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<User>> remove(@PathVariable long id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(userService.findAllUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "management/reports", method = RequestMethod.GET)
    public ResponseEntity<String> reports() {
        return new ResponseEntity<>("Some report data", HttpStatus.OK);
    }

}

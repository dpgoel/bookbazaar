package com.devpriya.user.controller;

import com.devpriya.user.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

@RestController
@RequestMapping("api/public")
@CrossOrigin
public class PublicRestApiController {
    private UserService userService;

    public PublicRestApiController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test1")
    public String test1() {
        return "API Test 1";
    }

    @GetMapping("/test2")
    public String test2() {
        return "API Test 2";
    }

}
